Rails.application.routes.draw do
  resources :usages
  resources :recipes
  resources :ingredients
end
