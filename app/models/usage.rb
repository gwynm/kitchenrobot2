#An ingredient that is used in a recipe. Note that we have no concept of 'quantity needed'.
class Usage < ApplicationRecord
	belongs_to :recipe
	belongs_to :ingredient
	validates :recipe_id,{uniqueness: {scope: :ingredient_id}}
end
