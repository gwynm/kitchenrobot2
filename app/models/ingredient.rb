class Ingredient < ApplicationRecord
		validates :name, {uniqueness: true}
		has_many :usages
		has_many :recipes, through: :usages		
end
