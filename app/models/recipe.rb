class Recipe < ApplicationRecord
		validates :name, {uniqueness: true}
		has_many :usages
		has_many :ingredients, through: :usages
end
